	$(document).ready(function(){
    $("#accordion tr.table__line:first").addClass("active");
    $("#accordion tr.table__line__info:not(:first)").hide();

    $("#accordion tr.table__line").click(function(){

        $(this).next("#accordion tr.table__line__info").slideToggle("slow")
        .siblings("#accordion tr.table__line__info:visible").slideUp("slow");
        $(this).toggleClass("active");
        $(this).siblings("#accordion tr.table__line").removeClass("active");
     });
 
 });
